#!/bin/bash

TOTAL=$(free -m | grep ^Mem: | tr -s " " | cut -d " " -f 2)
USED=$(free -m | grep ^Mem: | tr -s " " | cut -d " " -f 3)

AUX=$(($USED * 100))
PERCENTAGE=$(($AUX / $TOTAL))

if [ $PERCENTAGE -ge $2 ]
then
	echo Critical - Memory Used: $PERCENTAGE %
	exit 2
fi
if [ $PERCENTAGE -ge $1 ]
then
	echo Warning - Memory Used: $PERCENTAGE %
	exit 1
fi

echo OK - Memory Used: $PERCENTAGE %
exit 0

