#!/bin/bash

INTERFACE=$1
IN=$(ifconfig $INTERFACE | grep 'bytes' | cut -d ":" -f 2 | cut -d " " -f 1)
OUT=$(ifconfig $INTERFACE | grep 'bytes' | cut -d ":" -f 3 | cut -d " " -f 1)
FILE_LOG=/home/asist/lapr5/log3_$1.txt

if [ ! -e $FILE_LOG ]
then
	touch $FILE_LOG
	chmod 777 $FILE_LOG
	echo $IN >> $FILE_LOG
	echo $OUT >> $FILE_LOG
	echo Warning, criado primeiro registo
	exit 1
else
	LAST_OUT=$(tail -1 $FILE_LOG)
	LAST_IN=$(tail -2 $FILE_LOG | head -1)
	
	REAL_IN=$(($IN - $LAST_IN))
	REAL_OUT=$(($OUT - $LAST_OUT))

	echo OK: bytes received=$REAL_IN , bytes transmitted=$REAL_OUT
        echo $IN >> $FILE_LOG
	echo $OUT >> $FILE_LOG
        exit 0
fi

