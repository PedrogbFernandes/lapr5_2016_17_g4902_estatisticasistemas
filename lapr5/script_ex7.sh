#!/bin/bash
TABLE=$1
SQL_OUTPUT=$(echo "SELECT count(*) from $TABLE" | mysql portovisitas -u portovisitas -p'portovisitas')
CURRENT_ROWS=${SQL_OUTPUT#count(*)}
FILE_LOG=/home/asist/lapr5/log7$TABLE.txt

if [ ! -e $FILE_LOG ]
then
	touch $FILE_LOG
	chmod 777 $FILE_LOG
	echo $CURRENT_ROWS >> $FILE_LOG
	echo Criado registo com tamanho: $CURRENT_ROWS
	exit 2
else
	LAST_ENTRY=$(tail -1 $FILE_LOG)
	if [ $CURRENT_ROWS -ne $LAST_ENTRY ]
	then
		RESULT=$(expr $CURRENT_ROWS - $LAST_ENTRY)			
		echo Warning: Alteracao no tamanho da tabela: $RESULT, tamanho atual: $CURRENT_ROWS
		echo $CURRENT_ROWS >> $FILE_LOG
		exit 2
	fi
	echo OK: Tamanho da tabela continua igual: $CURRENT_ROWS
        exit 0
fi
