#!/bin/bash

RESULT=$(curl -Is $1 | head -1 )
STATUS=$(echo $RESULT | cut -d ' ' -f2)
echo $RESULT
if [ $STATUS -lt 400 ]
then
	echo WebService is up
	exit 0
else	
	echo WebService is down
	exit 1
fi

