#!/bin/bash

USERS_MAX=3
SQL_OUTPUT=$(echo "SELECT count(*) from AspNetUsers" | mysql portovisitas -u portovisitas -p'portovisitas')
CURRENT_ROWS=${SQL_OUTPUT#count(*)}
FILE_LOG=/home/asist/lapr5/log1.txt

if [ ! -e $FILE_LOG ]
then
	touch $FILE_LOG
	chmod 777 $FILE_LOG
	echo $CURRENT_ROWS >> $FILE_LOG
	echo Registo de $CURRENT_ROWS utilizadores
	exit 2
else
	LAST_ENTRY=$(tail -1 $FILE_LOG)
	RESULT=$(expr $CURRENT_ROWS - $LAST_ENTRY)
	if [ $RESULT -ge $USERS_MAX ]
	then
		echo Critico: $RESULT utilizadores desde da ultima vez
		echo $CURRENT_ROWS >> $FILE_LOG
		exit 2
	fi
	echo OK: $RESULT utilizadores desde da ultima vez
        echo $CURRENT_ROWS >> $FILE_LOG
        exit 0
fi
