#!/bin/bash

PROCESS=$1
RESULT=$(service $PROCESS status)
if [[ $RESULT =~ .*start/running.* ]] 
then
	echo $RESULT 
	ps aux | head -1 | tr -s " " | cut -d " " -f 1-4
	ps aux | grep ^$PROCESS | tr -s " " | cut -d " " -f 1-4
	exit 0
else
	echo $RESULT
	exit 2
fi
