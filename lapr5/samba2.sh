#!/bin/bash
SQL_OUTPUT=$(echo "SELECT UserName from AspNetUsers" | mysql portovisitas -u portovisitas -p'portovisitas')
COUNT=$(echo "SELECT count(*) from AspNetUsers" | mysql portovisitas -u portovisitas -p'portovisitas')
CURRENT_ROWS=${COUNT#count(*)}
USERLIST=$(getent passwd | grep 'PV_' | cut -d ':' -f 1)
USERLISTCOUNT=$(echo $USERLIST | wc -w)
SAMBAUSERS=$(sudo pdbedit -L -v | grep 'Unix username:' | wc -l)
echo Utilizadores locais: $USERLISTCOUNT
echo Utilizadores na BD : $CURRENT_ROWS
echo Utilizadores no samba: $SAMBAUSERS
for (( i=1; i <= $USERLISTCOUNT; ++i ))
do
    NAME=$(echo $USERLIST | cut -d ' ' -f $i)
    REALNAME=${NAME#PV_}
    if [[ $SQL_OUTPUT =~ $REALNAME ]]  
	then
	    echo $NAME existe
	else
	    echo $NAME já não existe, apagar referencia
            sudo smbpasswd -x $NAME
            sudo userdel -r -f $NAME
	fi
done

